#!/usr/bin/env python3.8

import sys

from scapy.layers.inet import IP, ICMP
from scapy.sendrecv import sr1


def main(host: str):
    for ttl in range(1, 17):
        packet = IP(dst=host, ttl=ttl)/ICMP()
        if not (response := sr1(packet, verbose=0, timeout=2)):
            print('Did not receive any response. Quitting')
            return
        elif response.src == host:
            print(f'{ttl}\t{response.src}\tdestination reached.')
            return
        else:
            print(f'{ttl}\t{response.src}')
    print('TTL over 16. Quitting.')


if __name__ == '__main__':
    main(sys.argv[1])
