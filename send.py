import socket, sys

HOST = 'localhost'
PORT = 4321

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sender:
    sender.connect((HOST, PORT))
    for char in sys.argv[1]:
        sender.sendall(b'\x01\x02\x03\x04\x05' + char.encode('utf-8'))
