#!/usr/bin/env python3

import sys

from scapy.config import conf
from scapy.layers.inet import IP, TCP
from scapy.sendrecv import sr1


def main(host: str):
    results = []
    conf.verb = 0
    for port in range(1, 101):
        print(f'scanning port {port}', end='\r')
        response = sr1(IP(dst=host)/TCP(dport=port, flags='S'), verbose=0, timeout=1)
        if response is not None and response.getlayer(TCP).flags == 'SA':
            results.append(f'{port}: OPEN')
    print(' '*80 + '\r')
    print('\n'.join(results))


if __name__ == '__main__':
    try:
        main(sys.argv[1])
    except IndexError:
        print('Usage: python3 nmap.py <host_addr>')
