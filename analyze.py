#!/usr/bin/env python3

from scapy.all import sniff, raw, IP, TCP, Packet


class Sniffer:
    def __init__(self):
        super().__init__()
        self._message = b''

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        print(self._message.decode('utf-8'))

    def run(self):
        sniff(
            offline='dump.pcapng',
            lfilter=self.my_filter,
            prn=self.display_filtered_packet
        )

    def display_filtered_packet(self, packet: Packet):
        raw_packet = raw(packet.payload)
        position = raw_packet.find(b'\x01\x02\x03\x04\x05')
        raw_packet = raw_packet[position:].replace(b'\x01\x02\x03\x04\x05', b'').replace(b'\r', b'')
        self._message += raw_packet

    def my_filter(self, packet: Packet) -> bool:
        return b'\x01\x02\x03\x04\x05' in raw(packet.payload)


if __name__ == '__main__':
    with Sniffer() as sniffer:
        sniffer.run()
